
const express = require('express')
const hbs=require('hbs')
const app = express()
const port=process.env.PORT || 3000
const path = require('path')
const weather=require('./routes/weather.js')
//middlewares
//view engines
app.set('view engine', 'hbs'); //template engines for pass paramter for html code  علشان نمرر دات لل html كود
hbs.registerPartials( path.join(__dirname,'./templates/partials'))//between htmls 
app.set('views', path.join(__dirname,'./templates/views'));
app.use(express.static(path.join(__dirname, './public'))) // middleware to load all assets for html code
app.use(weather)
////
app.listen(port)
//"nodemon -e * app.js"