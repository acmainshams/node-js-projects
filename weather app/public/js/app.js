//match the first element
//call apis from  Weather.js
console.log('Client side javascript file is loaded!')
const weatherForm = document.querySelector('form')
const search = document.querySelector('input')
const messageOne = document.querySelector('#messageOne')
const messageTwo = document.querySelector('#messageTwo')


weatherForm.addEventListener('submit', (e) => {
    e.preventDefault()// to prevent clear 
    const location = search.value
    messageOne.textContent="LOADING ......."
    messageTwo.textContent=""
      //console.log(location)
    fetch('/weather?address=' + location).then((response) => {
        response.json().then((data) => {
            //console.log(data)
            if (data.error) {
                messageOne.textContent=data.error
             
            } else {
                messageOne.textContent=data.location
                messageTwo.textContent=data.forecast
                //console.log(data.location)
                //console.log(data.forecast)          
            }
        })
    })
})
