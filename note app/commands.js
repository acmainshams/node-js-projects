const yargs=require('yargs');
const notes=require('./notes')
yargs.command(
    { 
        builder:{
            title: { //--title=""
                describe: 'note title', 
                demandOption: true,
                type: 'string' 
            }, 
            body: { //--title=""
                describe: 'note body', 
                demandOption: true,
                type: 'string' 
            }, 
        },
        
        command: 'add', 
        describe: 'Adds New Note', 
        handler:function(argv){
          notes["addNote"](argv.title,argv.body)
        
           // console.log("Adds New Note",argv.title)
        }
    }

)
////
yargs.command(
    { 
        builder:{
            title: { 
                describe: 'note title', 
                demandOption: true,
                type: 'string' 
            }, 
          
            
        },
        command: 'delete', 
        describe: 'Delete Note', 
  
        handler:function(argv){
          notes.deleteNote(argv.title)

        }
    }

)
////

yargs.command(
    { 
        builder:{
            title: { 
                describe: 'note title', 
                demandOption: true,
                type: 'string' 
            }, 
            
        },
        command: 'read', 
        describe: 'Read a Note', 

        handler:function(argv){
            notes.readNote(argv.title)
        }
}

)
////
yargs.command(
    { 
        command: 'list', 
        describe: 'List Your Notes', 
  
        handler:function(){
            notes.listNote()
        }
    }

)

yargs.parse() // To set above changes 

//module.exports=yargs