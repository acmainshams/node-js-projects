//u can considered as apis 
//requests 
const express = require('express')

const router = express.Router()

router.get('', (req, res) => {
    res.render('index', {
        title: 'Weather',
        name: 'Ahmed Salama'
    })
})

router.get('/about', (req, res) => {
    res.render('about', {
        title: 'About Me',
        name: 'Ahmed Salama'
    })
})

router.get('/help', (req, res) => {
    res.render('help', {
        helpText: 'This is some helpful text.',
        title: 'Help',
        name: 'Ahmed Salama'
    })
})
const forecast=require('../src/forecast')
const geocode = require('../src/geocode')
router.get('/weather', (req, res) => {

    if(!req.query.address){
        return res.send({"error":"You Must provide a address term"});
       }

geocode( req.query.address,(callbacks={})=>{//get cordinates of address lat ,lang
   // console.log(callbacks)
   forecast(callbacks["long"],callbacks["lat"],(callbacks={})=>{
       console.log(callbacks)
       res.send({
        forecast: callbacks,
        location: callbacks,
        address : req.query.address
    })
   })
})
////

})
router.get("/products",(req,res,next)=>{
if(!req.query.search){
 return res.send({"error":"You Must provide a search term"});
}

    res.send({"products":[]})


})

router.get('*', (req, res) => {  //hidden pass views/
       res.render('404', {title: '404',
       name: 'Ahmed Salama', 
       errorMessage: 'Page not found.'
    }) }) 

    
module.exports=router